package com.quadrado;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MatrixTest {

    private Matrix squareMatrix;
    private String filename;

    @Test(expected = IOException.class)
    public void isPerfectWithInexistentFile() throws Exception {
        squareMatrix = new Matrix("fake");
    }

    @Test(expected = NumberFormatException.class)
    public void isPerfectWithNonNumericElementsInFile() throws Exception {
        filename = getClass().getResource("/invalidMatrix.txt").getPath();
        squareMatrix = new Matrix(filename);
    }

    @Test(expected = IllegalStateException.class)
    public void isPerfectWithEmptyFile() throws Exception {
        filename = getClass().getResource("/emptyFile.txt").getPath();
        squareMatrix = new Matrix(filename);
    }

    @Test
    public void isNotPerfectMatrix() throws Exception {
        filename = getClass().getResource("/data.txt").getPath();
        squareMatrix = new Matrix(filename);
        assertFalse(squareMatrix.isPerfect());
    }

    @Test
    public void isNotSquareMatrix2X3() throws Exception {
        filename = getClass().getResource("/matrix2X3.txt").getPath();
        squareMatrix = new Matrix(filename);
        assertFalse(squareMatrix.isPerfect());
    }

    @Test
    public void isNotSquareMatrix3X2() throws Exception {
        filename = getClass().getResource("/matrix3X2.txt").getPath();
        squareMatrix = new Matrix(filename);
        assertFalse(squareMatrix.isPerfect());
    }

    @Test
    public void isPerfectAllSameNumber() throws Exception {
        isPerfectTest("/sameElements.txt");
    }

    @Test
    public void isPerfectOrder4() throws Exception {
        isPerfectTest("/perfectOrder4.txt");
    }

    @Test
    public void isPerfectOrder9() throws Exception {
        isPerfectTest("/perfectOrder9.txt");
    }

    private void isPerfectTest(final String filepath) throws Exception {
        filename = getClass().getResource(filepath).getPath();
        squareMatrix = new Matrix(filename);
        assertTrue(squareMatrix.isPerfect());
    }

}