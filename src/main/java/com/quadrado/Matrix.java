package com.quadrado;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Stream.of;

public class Matrix {

    private static final String WHITESPACE = "\\s+";

    private Integer[][] elements;
    private int order;
    private boolean square;
    private boolean allElementsAreEquals;

    public Matrix(final String filename) throws IOException {
        this.elements = loadMatrix(filename);
        this.order = elements.length;
        this.square = elements.length == elements[0].length;
    }

    private Integer[][] loadMatrix(final String filename) throws IOException {
        final List<String> lines;

        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            lines = stream.collect(Collectors.toList());
            this.allElementsAreEquals = lines.stream().distinct().count() == 1L;
        }

        validateContentFile(lines);

        return lines.stream().map(line ->
                of(line.split(WHITESPACE)).map(Integer::parseInt).toArray(Integer[]::new)).toArray(Integer[][]::new);
    }

    private void validateContentFile(final List<String> lines) {
        if (lines.isEmpty()) {
            throw new IllegalStateException("File cannot be empty");
        }
    }

    private boolean checkMatrix() {
        if (!this.square) {
            return false;
        }

        int totalLine;
        int totalColumn;
        int totalFirstDiagonal = 0;
        int totalSecondDiagonal = 0;
        int magicConstant = (order * order * order + order)/2;

        for (int i = 0; i < order; i++) {
            totalFirstDiagonal += elements[i][i];
            totalSecondDiagonal += elements[i][order - 1 -i];
            totalLine = totalColumn = 0;

            for (int j = 0; j < order; j++) {
                totalLine += elements[i][j];
                totalColumn += elements[j][i];
            }

            if ( totalLine != magicConstant || totalColumn != magicConstant )
                return false;
        }

        return  (totalFirstDiagonal == magicConstant && totalSecondDiagonal == magicConstant);
    }

    public boolean isPerfect() {
        return this.square && allElementsAreEquals || checkMatrix();
    }

}
