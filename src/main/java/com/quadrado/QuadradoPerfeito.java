package com.quadrado;

public class QuadradoPerfeito {

    public static void main(String... args) {
        checkArguments(args);

        try {
            final Matrix matrix = new Matrix(args[0]);
            final boolean perfect = matrix.isPerfect();
            System.out.println("Quadrado e perfeito? " + perfect);
        } catch (Exception e) {
            System.out.printf("O arquivo %s não foi encontrado ou contem dado(s) invalido(s)!%n", args[0]);
        }

    }

    private static void checkArguments(final String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java QuadradoPerfeito <arquivo de entrada>");
            System.exit(0);
        }
    }
}
