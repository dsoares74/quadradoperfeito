# Quadrado perfeito

* A square of size NxN is perfect if the summing the numbers in each row, each column and in the forward and backward main diagonals are the same number.


### How do I get set up? ###

* Dependencies
To get the Dependencies, simply run the following command:

```sh
$ mvn clean package
```


# Unit Tests

* We are using JUnit for unit tests.

* To run, execute the following steps in terminal:

```sh
$ mvn test
```


To start the application, you can run the following command:

```sh
mvn -q clean compile exec:java -Dexec.mainClass="com.quadrado.QuadradoPerfeito" -Dexec.args="src/test/resources/data.txt"
```